# React Game

Documentation of this activity can be found at [DAW-6-UF3-2 - React](https://docs.google.com/document/d/1y3aLfR3Dmhc6af-7P7baW6aKxHUkogPHcnIk3xCh56Q/edit?usp=sharing)

You can run container from image:

```sh
docker run --rm --name game -p 80:80 registry.gitlab.com/xtec/react-game
```


## Develop

Run the development server:

```bash
npm install
npm run dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

## Build

Install docker: 

```sh
sudo apt update
sudo apt install -y docker.io
```

Build a docker image:

```sh
docker build -t xtec/react-game .
```

Run the xtec/react-game image:

```sh
docker run --rm -d --name game -p 3000:3000 xtec/react-game
```


