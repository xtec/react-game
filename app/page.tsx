'use client';
import { useState } from "react";

export default function Game() {

  const [history, setHistory] = useState([Array(9).fill(null)]);
  const [currentMove, setCurrentMove] = useState(0)

  const xIsNext = currentMove % 2 == 0;
  const currentSquares = history[currentMove];

  function handlePlay(nextSquares: Array<number>) {
    const nextHistory = [...history.slice(0, currentMove + 1), nextSquares];
    setHistory(nextHistory);
    setCurrentMove(nextHistory.length - 1);;
  }

  function jumpTo(nextMove: number) {
    setCurrentMove(nextMove);
  }

  const moves = history.map((squares, move) => {
    let description;
    if (move > 0) {
      description = "Anar al movimient #" + move;
    } else {
      description = "Anar a l'inici del joc";
    }
    return (
      <li key={move}>
        <button onClick={() => jumpTo(move)}>{description}</button>
      </li>
    );
  });

  return (
    <div className="game">
      <div className="game-board">
        <Board xIsNext={xIsNext} squares={currentSquares} onPlay={handlePlay} />
      </div>
      <div className="game-info">
        <ol>{moves}</ol>
      </div>
    </div>
  );
}

interface BoardProps {
  xIsNext: boolean,
  squares: Array<string>
  onPlay: any
}

function Board({ xIsNext, squares, onPlay }: BoardProps) {

  const winner = calculateWinner(squares);
  let status;
  if (winner) {
    status = "Guanyador: " + winner;
  } else {
    status = "Següent jugador: " + (xIsNext ? "X" : "O");
  }

  function handleClick(i: number) {

    if (squares[i] || calculateWinner(squares))
      return

    const nextSquares = squares.slice();
    if (xIsNext)
      nextSquares[i] = "X"
    else
      nextSquares[i] = "O"

    onPlay(nextSquares);
  }


  return (
    <div className="container m-2">
      <div className="row">
        <div className="col text-center p-5 fs-3">{status}</div>
      </div>
      <div className="row">
        <div className="col" />
        <Square value={squares[0]} onSquareClick={() => handleClick(0)} />
        <Square value={squares[1]} onSquareClick={() => handleClick(1)} />
        <Square value={squares[2]} onSquareClick={() => handleClick(2)} />
        <div className="col" />
      </div>
      <div className="row">
        <div className="col" />
        <Square value={squares[3]} onSquareClick={() => handleClick(3)} />
        <Square value={squares[4]} onSquareClick={() => handleClick(4)} />
        <Square value={squares[5]} onSquareClick={() => handleClick(5)} />
        <div className="col" />
      </div>
      <div className="row">
        <div className="col" />
        <Square value={squares[6]} onSquareClick={() => handleClick(6)} />
        <Square value={squares[7]} onSquareClick={() => handleClick(7)} />
        <Square value={squares[8]} onSquareClick={() => handleClick(8)} />
        <div className="col" />
      </div>
    </div>
  )
}

interface SquareProps {
  value: string,
  onSquareClick: any
}

function Square({ value, onSquareClick }: SquareProps) {

  return <div onClick={onSquareClick} className="col-3 col-md-2 p-3 border fs-1 fw-bold text-center">{value}</div>
}

function calculateWinner(squares: Array<string>) {
  const lines = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6]
  ];
  for (let i = 0; i < lines.length; i++) {
    const [a, b, c] = lines[i];
    if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
      return squares[a];
    }
  }
  return null;
}